<?php

namespace CloudZentral\Bambora\Providers;

/**
 * Class ServiceProvider
 * @package CloudZentral\Bambora\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
