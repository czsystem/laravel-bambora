<?php

namespace CloudZentral\Bambora;

use Carbon\Carbon;

/**
 * Class Transaction
 * @package CloudZentral\Bambora
 */
class Transaction
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    public $group;

    /**
     * @var int
     */
    public $authAmount;

    /**
     * @var int
     */
    public $currency;

    /**
     * @var int
     */
    public $cardTypeId;

    /**
     * @var int
     */
    public $capturedAmount;

    /**
     * @var int
     */
    public $creditedAmount;

    /**
     * @var string
     */
    public $orderId;

    /**
     * @var string
     */
    public $description;

    /**
     * @var Carbon
     */
    public $authDate;

    /**
     * @var Carbon
     */
    public $capturedDate;

    /**
     * @var Carbon
     */
    public $deletedDate;

    /**
     * @var Carbon
     */
    public $creditedDate;

    /**
     * @var string
     */
    public $status;

    /**
     * @var int
     */
    public $transactionId;

    /**
     * @var string
     */
    public $cardHolder;

    /**
     * @var string
     */
    public $acquirer;

    /**
     * @var string
     */
    public $tCardNo;

    /**
     * @var int
     */
    public $expireMonth;

    /**
     * @var int
     */
    public $expireYear;

    /**
     * @var Carbon
     */
    public $expire;

    /**
     * Transaction constructor.
     * @param Client $client
     * @param string $group
     * @param int $authAmount
     * @param int $currency
     * @param int $cardTypeId
     * @param int $capturedAmount
     * @param int $creditedAmount
     * @param string $orderId
     * @param string $description
     * @param string $authDate
     * @param string $capturedDate
     * @param string $deletedDate
     * @param string $creditedDate
     * @param string $status
     * @param int $transactionId
     * @param string $cardHolder
     * @param string $acquirer
     * @param string $tCardNo
     * @param int $expireMonth
     * @param int $expireYear
     */
    public function __construct(
        Client $client,
        string $group,
        int $authAmount,
        int $currency,
        int $cardTypeId,
        int $capturedAmount,
        int $creditedAmount,
        string $orderId,
        string $description,
        string $authDate,
        string $capturedDate,
        string $deletedDate,
        string $creditedDate,
        string $status,
        int $transactionId,
        string $cardHolder,
        string $acquirer,
        string $tCardNo,
        int $expireMonth,
        int $expireYear
    )
    {
        $this->client = $client;
        $this->group = $group;
        $this->authAmount = $authAmount;
        $this->currency = $currency;
        $this->cardTypeId = $cardTypeId;
        $this->capturedAmount = $capturedAmount;
        $this->creditedAmount = $creditedAmount;
        $this->orderId = $orderId;
        $this->description = $description;
        $this->authDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $authDate);
        $this->capturedDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $capturedDate);
        $this->deletedDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $deletedDate);
        $this->creditedDate = Carbon::createFromFormat('Y-m-d\TH:i:s', $creditedDate);
        $this->status = $status;
        $this->transactionId = $transactionId;
        $this->cardHolder = $cardHolder;
        $this->acquirer = $acquirer;
        $this->tCardNo = $tCardNo;
        $this->expireMonth = $expireMonth;
        $this->expireYear = $expireYear;
        $this->expire = Carbon::create("20".$expireYear, $expireMonth);
    }

    /**
     * Capture payment.
     * Payment is deducted.
     * @param int|null $amount
     * @param bool $invoice
     * @return bool
     * @throws Exceptions\ResponseErrorException
     */
    public function capture(int $amount = null, bool $invoice = false): bool
    {
        return $this->client->capturePayment($this, $amount, $invoice);
    }

    /**
     * Credit payment.
     * The amount is returned.
     * This can only be called after a successful capture.
     * @param int|null $amount
     * @param bool $invoice
     * @return bool
     * @throws Exceptions\ResponseErrorException
     */
    public function credit(int $amount = null, bool $invoice = false): bool
    {
        return $this->client->creditPayment($this, $amount, $invoice);
    }

    /**
     * Delete payment.
     * The payment is canceled and the reserved amount is released.
     * This can only be called if the transaction has not been captured.
     * @return bool
     * @throws Exceptions\ResponseErrorException
     */
    public function delete(): bool
    {
        return $this->client->deletePayment($this);
    }
}
