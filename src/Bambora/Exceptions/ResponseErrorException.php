<?php

namespace CloudZentral\Bambora\Exceptions;

use CloudZentral\Bambora\Client;
use Throwable;

/**
 * Class ResponseErrorException
 * @package CloudZentral\Bambora\Exceptions
 */
class ResponseErrorException extends \Exception
{
    /**
     * ResponseErrorException constructor.
     * @param Client $client
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(Client $client, $code = 0, Throwable $previous = null)
    {
        if($code > 0) {
            // PBS
            $message = "PBS error with code ".$code;
        } else if($code < 0) {
            // Payment system (epay)
            $message = "Epay error with code ".$code;
        } else {
            // Unknown error code
            $message = "Unknown error code ".$code;
        }
        parent::__construct($message, $code, $previous);
    }

}
