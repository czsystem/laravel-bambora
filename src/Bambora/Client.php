<?php

namespace CloudZentral\Bambora;

use CloudZentral\Bambora\Exceptions\ResponseErrorException;
use SoapClient;
use SoapFault;

/**
 * Class Client
 * @package CloudZentral\Bambora
 */
class Client
{
    /**
     * @var string $merchantNumber
     */
    private $merchantNumber;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var $soapClient
     */
    private $soapClient;

    /**
     * @var string $wsdl
     */
    private $wsdl = 'https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx?wsdl';

    /**
     * @var int $language
     */
    private $language;

    /**
     * Client constructor.
     * @param string $merchantNumber
     * @param string $password
     * @param int $language
     * @throws SoapFault
     */
    public function __construct(string $merchantNumber, string $password = "", int $language = 2)
    {
        $this->merchantNumber = $merchantNumber;
        $this->password = $password;
        $this->language = $language;
        $this->soapClient = $this->getSoapClient();
    }

    /**
     * Get soap client.
     * @return SoapClient
     * @throws SoapFault
     */
    private function getSoapClient(): SoapClient
    {
        return new SoapClient($this->wsdl);
    }

    /**
     * Get transaction.
     * @param string $transactionId
     * @return Transaction
     * @throws ResponseErrorException
     */
    public function getTransaction(string $transactionId): Transaction
    {
        $soapResponse = $this->soapClient->getTransaction([
            'merchantnumber'    => $this->merchantNumber,
            'transactionid'     => $transactionId,
            'pwd'               => $this->password,
            'epayresponse'      => '-1'
        ]);
        $this->handleResponseErrors($soapResponse);

        $info = $soapResponse->transactionInformation;
        return new Transaction(
            $this,
            $info->group,
            $info->authamount,
            $info->currency,
            $info->cardtypeid,
            $info->capturedamount,
            $info->creditedamount,
            $info->orderid,
            $info->description,
            $info->authdate,
            $info->captureddate,
            $info->deleteddate,
            $info->crediteddate,
            $info->status,
            $info->transactionid,
            $info->cardholder,
            $info->acquirer,
            $info->tcardno,
            $info->expmonth,
            $info->expyear
        );
    }

    /**
     * Capture payment.
     * Payment is deducted.
     * @param string|Transaction $transaction
     * @param int|null $amount
     * @param bool $invoice
     * @return bool
     * @throws ResponseErrorException
     */
    public function capturePayment($transaction, int $amount = null, bool $invoice = false): bool
    {
        if(is_string($transaction)) {
            $transaction = $this->getTransaction($transaction);
        }

        $amount = is_null($amount) ? $transaction->authAmount : $amount;

        $soapResponse = $this->soapClient->capture([
            'merchantnumber'    => $this->merchantNumber,
            'transactionid'     => $transaction->transactionId,
            'amount'            => $amount,
            'group'             => $transaction->group,
            'pwd'               => $this->password,
            'invoice'           => ( $invoice ? $invoice : '-1' ),
            'orderid'           => $transaction->orderId,
            'pbsResponse'       => '-1',
            'epayresponse'      => '-1'
        ]);
        $this->handleResponseErrors($soapResponse);
        return true;
    }

    /**
     * Credit payment.
     * The amount is returned.
     * This can only be called after a successful capture.
     * @param string|Transaction $transaction
     * @param int|null $amount
     * @param bool $invoice
     * @return bool
     * @throws ResponseErrorException
     */
    public function creditPayment($transaction, int $amount = null, bool $invoice = false): bool
    {
        if(is_string($transaction)) {
            $transaction = $this->getTransaction($transaction);
        }

        $amount = is_null($amount) ? $transaction->authAmount : $amount;

        $soapResponse = $this->soapClient->credit([
            'merchantnumber'    => $this->merchantNumber,
            'transactionid'     => $transaction->transactionId,
            'amount'            => $amount,
            'group'             => $transaction->group,
            'pwd'               => $this->password,
            'invoice'           => ( $invoice ? $invoice : '-1' ),
            'orderid'           => $transaction->orderId,
            'pbsresponse'       => '-1',
            'epayresponse'      => '-1'
        ]);
        $this->handleResponseErrors($soapResponse);
        return true;
    }

    /**
     * Delete payment.
     * The payment is canceled and the reserved amount is released.
     * This can only be called if the transaction has not been captured.
     * @param string|Transaction $transaction
     * @return bool
     * @throws ResponseErrorException
     */
    public function deletePayment($transaction): bool
    {
        if(is_string($transaction)) {
            $transaction = $this->getTransaction($transaction);
        }

        $soapResponse = $this->soapClient->delete([
            'merchantnumber'    => $this->merchantNumber,
            'transactionid'     => $transaction->transactionId,
            'amount'            => $transaction->authAmount,
            'group'             => $transaction->group,
            'pwd'               => $this->password,
            'pbsresponse'       => '-1',
            'epayresponse'      => '-1'
        ]);
        $this->handleResponseErrors($soapResponse);
        return true;
    }

    /**
     * Handle response errors.
     * @param \stdClass $soapResponse
     * @throws ResponseErrorException
     */
    private function handleResponseErrors(\stdClass $soapResponse)
    {
        if(isset($soapResponse->pbsResponse) && $soapResponse->pbsResponse != -1 && $soapResponse->pbsResponse != 0) {
            throw new ResponseErrorException($this, $soapResponse->pbsResponse);
        } else if(isset($soapResponse->pbsresponse) && $soapResponse->pbsresponse != -1 && $soapResponse->pbsresponse != 0) {
            throw new ResponseErrorException($this, $soapResponse->pbsresponse);
        } else if(isset($soapResponse->epayresponse) && $soapResponse->epayresponse != -1 && $soapResponse->epayresponse != 0) {
            throw new ResponseErrorException($this, $soapResponse->epayresponse);
        }
    }
}
